#include "main.h"

int niveau_termine(niveau_t* niveau)
{
	//1 = fin de la partie   &   0 = partie pas fini
	int res = 1;
	for(int i = 0; i<niveau->nb_lignes; i++){
		for(int u = 0; u<niveau->nb_colonnes; u++){
			if(lecture_du_terrain(niveau, u, i )=='$') res = 0;
		}
	}
	return res;
}