# Sokoban

Quentin BERNARD

## Description du projet

Sokoban est un projet d'informatique réalisé lors du 2ème semestre de DUT Informatique. Ce projet a été développé en langage C et s'inspire du célèbre jeu du Sokoban.

Le Sokoban est un jeu de réflexion dans lequel le joueur incarne un personnage devant déplacer des caisses dans un entrepôt afin de les positionner sur des emplacements spécifiques. Le défi réside dans la recherche de la meilleure stratégie pour déplacer les caisses et résoudre les différents niveaux du jeu.

Le projet Sokoban a été conçu dans le but de synthétiser les connaissances acquises tout au long du semestre. Il a été une occasion pour mettre en pratique les concepts et les compétences développés, tels que la manipulation des structures de données, l'utilisation des boucles, les conditions, les fonctions, etc.

Vidéo de présentation : lien

## Fonctionnalités

1 - Génération du plateau :
Le jeu peut générer un plateau de jeu à partir d'un fichier texte prédéfini.
Le fichier texte contient la disposition des murs, des emplacements pour les caisses et du personnage.

2 - Déplacement du personnage et des caisses :
Le joueur peut déplacer son personnage sur le plateau.
Le personnage peut pousser les caisses, une à la fois, dans les directions disponibles.
Les mouvements sont contraints par les murs et les obstacles présents sur le plateau.

3 - Historique des coups précédents :
Le jeu conserve un historique des coups précédemment joués.
Le joueur a la possibilité d'annuler ses déplacements précédents et de revenir en arrière dans le jeu.

4 - Détection de la fin de la partie :
Le jeu détecte la fin de la partie lorsque toutes les caisses sont correctement positionnées sur les emplacements prévus.
Une fois la fin de la partie détectée, le joueur peut passer au niveau suivant ou quitter le jeu.

Cependant, une fonctionnalité qui n'a pas été implémentée est le tableau des scores par ordre décroissant, tel que demandé dans le projet initial. Cette fonctionnalité n'a pas pu être intégrée à la version finale du jeu Sokoban.

