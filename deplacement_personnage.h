point_t* perso(int ligne, int colonne);
char entree_du_joueur(void);
void deplacement (niveau_t* niveau, char direction);
char majuscule (char c);
int caseSuivante(niveau_t* niveau, char direction, int colonne, int ligne, char typeCase);
int caseDevantLaCaisse(niveau_t* niveau, char direction, int colonne, int ligne, char typeCase);
void avancerJoueur(niveau_t* niveau, char direction, int playerX, int playerY, char caseSuivante, char ancienneCase);
point_t* refreshCoordonnesPerso(niveau_t* niveau, char direction);
void avancerJoueurEtCaisse(niveau_t* niveau, char direction, int playerX, int playerY, char caseSuivante, char ancienneCase, char caisse);