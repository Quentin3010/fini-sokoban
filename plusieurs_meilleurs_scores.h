char* nom_du_joueur(void);
void ecriture_du_multiscore(int quel_niveau, int score, char* pseudo);
void tri_du_fichier(int quel_niveau);
void tronquer_fichier(int quel_niveau);
int score_check(int quel_niveau, niveau_t* niveau);