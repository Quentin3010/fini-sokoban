niveau_t* copie_du_niveau(niveau_t* niveau);
historique_t* nouvel_historique();
void initialiser_historique(historique_t* hist);
void affichage_historique(historique_t* hist);
void liberation_historique(niveau_t** hist);
void sauvegarde_un_coup(historique_t* hist, niveau_t* niveau);
niveau_t* coup_precedent(historique_t* hist);
