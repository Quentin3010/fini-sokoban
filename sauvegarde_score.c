#include "main.h"

int lecture_du_score(int quel_niveau)
{
	//Obtention du chemin vers le fichier score
	char chemin[500];
  	sprintf(chemin,"./Scores/score_%d",quel_niveau);
  	//Ouverture du fichier
    FILE* fichier = fopen(chemin,"r");
    //Récupération du score sous forme de chaine de char
	char score[100];
	char car = getc(fichier);
	int i = 0;
    while (car != EOF) 
    {
    	score[i] = car;
    	car = getc(fichier);
    	if(car!=EOF) i++;
    }
    fclose(fichier);
    //Conersion de la chain de char en int puis on l'a retourne
    return char_to_int(score, i);
}

int char_to_int(char* nombre, int nb_chiffre)
{
	int res = 0;
	for(int u = 0; u<nb_chiffre; u++)
	{
		int puissance = 1;
		for(int i = nb_chiffre-u-2; i>=0; i--)
		{
			puissance = puissance * 10;
		}
		res += (nombre[u]-'0')*puissance;
	}
	return res;
}

void ecriture_du_score(int quel_niveau, int score)
{
	if(lecture_du_score(quel_niveau)>score)
	{
		printf("(Vous avez battu l'ancien meilleur score qui était à %d mouvements en faisant %d mouvements !!\n\n", lecture_du_score(quel_niveau), score);
		//Obtention du chemin vers le fichier score
		char chemin[500];
  		sprintf(chemin,"./Scores/score_%d",quel_niveau);
  		//Ouverture du fichier
    	FILE* fichier = fopen(chemin,"w");
    	//On écrit le nouveau score
    	fprintf(fichier,"%d\n",score);
    	//On ferme le fichier
		fclose(fichier);
	}
	else
	{
		printf("Vous n'avez pas battu le meilleur score qui est à %d mouvements.\n\n", lecture_du_score(quel_niveau));
	}
}