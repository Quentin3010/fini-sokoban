#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

typedef struct {	
    int colonne;
	int ligne;
} point_t;

typedef struct {
	char* terrain;
	int nb_colonnes;
	int nb_lignes;
    point_t* perso;
    int nb_de_pas;
}niveau_t;

typedef struct {
	char* pseudos;
	int* scores;
}score_t;

typedef struct {
    niveau_t** tableau;
    int nb_de_tour;
}historique_t;

#include "generation_niveau.h"
#include "deplacement_personnage.h"
#include "fin_jeu.h"
#include "sauvegarde_score.h"
#include "plusieurs_meilleurs_scores.h"
#include "historique_partie.h"

void menu(void);
void affiche_tables_des_scores(void);