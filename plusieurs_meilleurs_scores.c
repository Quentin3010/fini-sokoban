#include "main.h"

char* nom_du_joueur(void)
{
	char* pseudo;
	pseudo = malloc(8+1);
	scanf("%8s", pseudo);
	return pseudo;
}

void ecriture_du_multiscore(int quel_niveau, int score, char* pseudo){
    char chaine[200];

    FILE* fichier;
    sprintf(chaine, "./Plusieurs_meilleurs_scores/score_multi_%d",quel_niveau);
    fichier = fopen(chaine, "a");
    fprintf(fichier,"%d %s \n",score, pseudo);
    fclose(fichier);
    
}

void tri_du_fichier(int quel_niveau){
    char chaine[200];
    sprintf(chaine, "sort -g Plusieurs_meilleurs_scores/score_multi_%d | sort -u -t' ' -k2,2 | sort -g -o Plusieurs_meilleurs_scores/score_multi_%d ",quel_niveau, quel_niveau);
    // -u pour unique, -t pour delimiter = ' ' , -k2,2 pour field 2 
    //on prend seulement les lignes avec pseudos unique après avoir sort -g (pour etre sur d'éliminer les pires scores)
    //> ne fonctionne pas :This is because the redirection is carried out first: >file truncates your file so that the sort finds nothing
    //-o fait ce qu'on attendrai de 'sort -g Scores/score_multi_%d > Scores/score_multi_%d'
    system(chaine);

}

void tronquer_fichier(int quel_niveau){//forcement à utiliser après avoir trié
    char chaine[200];
    char chaine2[200];
    sprintf(chaine,"head -n 5 Plusieurs_meilleurs_scores/score_multi_%d >Plusieurs_meilleurs_scores/tmp", quel_niveau);
    sprintf(chaine2, "mv -f Plusieurs_meilleurs_scores/tmp Plusieurs_meilleurs_scores/score_multi_%d ",quel_niveau);
    system(chaine);
    system(chaine2);
}

int score_check(int quel_niveau, niveau_t* niveau){                        
    char chaine[200];  
    char chaine2[200];
    char chaine3[200];
    int pire_score = 0;

    sprintf(chaine,"tail -n 1 Plusieurs_meilleurs_scores/score_multi_%d | cut -d' ' -f1 >Plusieurs_meilleurs_scores/tmp", quel_niveau);
    sprintf(chaine2, "rm Plusieurs_meilleurs_scores/tmp");
    sprintf(chaine3, "Plusieurs_meilleurs_scores/tmp");
    system(chaine);

    FILE* file;
    file = fopen(chaine3,"r");
    fscanf(file, "%d",&pire_score);
    fclose(file);
    system(chaine2);
    
    if (pire_score>niveau->nb_de_pas)
    {
        return 1;
    }
    return 0;
}