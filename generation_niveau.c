#include "main.h"

#define couleur(param) printf("\033[%sm",param)

niveau_t* nouveau_niveau(int nb_colonnes, int nb_lignes)
{
	//Affectation du niveau à la mémoire
	niveau_t* niveau = malloc(sizeof(niveau_t)*1);
	//Affectation des différents paramètres du terrain
	niveau->nb_colonnes = nb_colonnes;
	niveau->nb_lignes = nb_lignes;
	niveau->terrain = malloc(sizeof(char)*nb_lignes*nb_colonnes);
	//Affectation des différents paramètres du joueur
	point_t* perso = malloc(sizeof(point_t)*1);
	perso->colonne = 0;
	perso->ligne = 0;
	niveau->perso = perso;
	//Initialisation du nombre de pas
	niveau->nb_de_pas = 0;
	return niveau;
}

void place_sur_terrain(niveau_t* niveau, int colonne, int ligne, char car)
{
	*(niveau->terrain+(ligne*niveau->nb_colonnes+colonne)) = car;
}

char lecture_du_terrain(niveau_t* niveau, int colonne, int ligne)
{
	return *(niveau->terrain + (niveau->nb_colonnes)*ligne+colonne);
}

void initialise_le_terrain (niveau_t* niveau)
{
	for(int i = 0; i<niveau->nb_lignes; i++)
	{
		for(int u = 0; u<niveau->nb_colonnes; u++)
		{
			place_sur_terrain (niveau, u, i, '#');
		}
	}
}

void affichage_niveau(niveau_t* niveau)
{
	for(int i = 0; i<niveau->nb_lignes; i++)
	{
		for(int u = 0; u<niveau->nb_colonnes; u++)
		{
			//Affiche le joueur (@) en rouge
			if(lecture_du_terrain (niveau, u, i)=='@')
			{
				couleur("31"); printf("@"); couleur("0");
			}
			//Affiche le joueur sur un point de rangement (+) en rouge
			else if(lecture_du_terrain (niveau, u, i)=='+')
			{
				couleur("31"); printf("+"); couleur("0");
			}
			//Affiche les caisses ($) en vert
			else if(lecture_du_terrain (niveau, u, i)=='$')
			{
				couleur("32"); printf("$"); couleur("0");
			}
			//Affiche les point de rangement (.) en bleu
			else if(lecture_du_terrain (niveau, u, i)=='.')
			{
				couleur("34"); printf("."); couleur("0");
			}
			//Affiche les point de rangement complet (*) en bleu
			else if(lecture_du_terrain (niveau, u, i)=='*')
			{
				couleur("34"); printf("*"); couleur("0");
			}
			//Affiche le reste en noir
			else printf("%c",lecture_du_terrain (niveau, u, i));
		}
		printf("\n");
	}
	printf("\n");
}

niveau_t* lecture_du_niveau(int quel_niveau){
	//Création de l'appelation du chemin du niveau souhaité
  	char chemin[500];
  	sprintf(chemin,"./Niveaux/niveau_%d",quel_niveau);
    //Ouverture du fichier
    FILE* fichier = fopen(chemin,"r");
    //Récupération du nombre de colonne
    int x = (getc(fichier) - '0')*10 + (getc(fichier) - '0');
    //Récupération du nombre de ligne
    getc(fichier);
    int y = (getc(fichier) - '0')*10 + (getc(fichier) - '0');
    //Affectation du niveau en mémoire
    niveau_t* niveau = nouveau_niveau(x,y);
    //Récupération des coordonnés du joueur dans le niveau de base
    getc(fichier);
    int colonne = (getc(fichier) - '0')*10 + (getc(fichier) - '0');
    getc(fichier);
    int ligne = (getc(fichier) - '0')*10 + (getc(fichier) - '0');
    niveau->perso = perso(ligne,colonne);
    //Création du niveau
    for(int i = 0; i<y; i++){
    	for(int u = 0; u<x; u++){
    		char c = getc(fichier);
    		if(c!='\n') place_sur_terrain(niveau,u,i,c);
    		else u--;
    	}
    }
    //On ferme le fichier et on retourne le niveau
    fclose(fichier);
    return niveau;
}

void liberation_du_niveau(niveau_t* niveau){
	free(niveau);
}
