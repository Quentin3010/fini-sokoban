#include "main.h"

point_t* perso(int ligne, int colonne)
{
	point_t* player = malloc(sizeof(point_t));
	player->ligne = ligne;
	player->colonne = colonne;

	return player;
}

char entree_du_joueur(void)
{
	char input=' ';
	printf("direction ?\n");
	while(input!= 'z' && input!='q' && input!='s' && input!='d' && input!='a'){
    	scanf("%c",&input);
    }
    return input;
}

void deplacement(niveau_t* niveau, char direction)
{
	int playerX = niveau->perso->colonne;
	int playerY = niveau->perso->ligne;
	system("clear");
	////////////////////////////
	//On vérifie que la case dans la direction souhaité est vide
	////////////////////////////
	if(caseSuivante(niveau, direction, playerX, playerY, ' ')==1)
	{
		//On vérifie ensuite si la case sur laquel on est est représenter par un + (soit une zone de rangement)
		if(lecture_du_terrain(niveau, playerX, playerY)=='+')
		{
			//on avance le joueur dans la direction '@'
			//replacer l'ancienne position du joueur par un ' '
			avancerJoueur(niveau, direction, playerX, playerY, '@', '.');
		}
		else
		{
			//on avance le joueur dans la direction '@'
			//replacer l'ancienne position du joueur par un '.'
			avancerJoueur(niveau, direction, playerX, playerY, '@', ' ');
		}
	}
	////////////////////////////
	//Sinon on vérifie que la case dans la direction souhaité est une caisse
	////////////////////////////
	else if(caseSuivante(niveau, direction, playerX, playerY, '$')==1)
	{
		//On vérifie ensuite que la casse devant la caisse est vide
		if(caseDevantLaCaisse(niveau, direction, playerX, playerY, ' ')==1)
		{
			//On vérifie ensuite si la case sur laquel on est est représenter par un + (soit une zone de rangement)
			if(lecture_du_terrain(niveau, playerX, playerY)=='+')
			{
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un ' '
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '@', '.', '$');
			}
			else
			{;
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un '.'
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '@', ' ', '$');
			}
		}
		//Sinon on vérifie que la case devant la caisse est une case de rangement
		else if(caseDevantLaCaisse(niveau, direction, playerX, playerY, '.')==1)
		{
			//On vérifie ensuite si la case sur laquel on est est représenter par un + (soit une zone de rangement)
			if(lecture_du_terrain(niveau, playerX, playerY)=='+')
			{
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un ' '
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '@', '.', '*');
			}
			else
			{
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un '.'
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '@', ' ', '*');
			}
		}
	}
	////////////////////////////
	//Sinon on vérifie que la case dans la direction souhaité est une casse de rangement
	////////////////////////////
	else if(caseSuivante(niveau, direction, playerX, playerY, '.')==1)
	{
		//On vérifie ensuite si la case sur laquel on est est représenter par un + (soit une zone de rangement)
		if(lecture_du_terrain(niveau, niveau->perso->colonne, niveau->perso->ligne)=='+')
		{
			//on avance le joueur dans la direction '@'
			//replacer l'ancienne position du joueur par un ' '
			avancerJoueur(niveau, direction, playerX, playerY, '+', '.');
		}
		else
		{
			//on avance le joueur dans la direction '@'
			//replacer l'ancienne position du joueur par un '.'
			avancerJoueur(niveau, direction, playerX, playerY, '+', ' ');
		}
	////////////////////////////
	//Sinon on vérifie que la case devant nous est une boite qui est sur une case de rangement = '*'
	////////////////////////////
	}
	else if(caseSuivante(niveau, direction, playerX, playerY, '*')==1)
	{
		//On vérifie ensuite que la casse devant la caisse est vide
		if(caseDevantLaCaisse(niveau, direction, playerX, playerY, ' ')==1)
		{
			//On vérifie ensuite si la case sur laquel on est est représenter par un + (soit une zone de rangement)
			if(lecture_du_terrain(niveau, playerX, playerY)=='+')
			{
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un ' '
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '+', '.', '$');
			}
			else
			{;
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un '.'
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '+', ' ', '$');
			}
		}
		//Sinon on vérifie que la case devant la caisse est une case de rangement
		else if(caseDevantLaCaisse(niveau, direction, playerX, playerY, '.')==1)
		{
			//On vérifie ensuite si la case sur laquel on est est représenter par un + (soit une zone de rangement)
			if(lecture_du_terrain(niveau, playerX, playerY)=='+')
			{
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un ' '
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '+', '.', '*');
			}
			else
			{
				//on avance le joueur dans la direction '@'
				//replacer l'ancienne position du joueur par un '.'
				avancerJoueurEtCaisse(niveau, direction, playerX, playerY, '+', ' ', '*');
			}
		}
	}
	////////////////////////////
	//Dans ce cas, il est impossible de bouger, donc rien ne change
	////////////////////////////
	else
	{
		place_sur_terrain(niveau, playerX, playerY, '@');
	}
}

//Fonction servant à vérifier que le type de case passé en paramètre correspond à la case trouver avec les coordonnés + la direction du joueur passé en paramètre
int caseSuivante(niveau_t* niveau, char direction, int colonne, int ligne, char typeCase)
{
	int playerX = colonne;
	int playerY = ligne;
	int res = 0;
	if(direction == 'z' && playerY-1>0 && lecture_du_terrain(niveau, playerX, playerY-1) == typeCase)
	{
			res = 1;
	}
	else if(direction == 's' && playerY+1<(niveau->nb_lignes) && lecture_du_terrain(niveau, playerX, playerY+1) == typeCase)
	{
			res = 1;
	}
	else if(direction == 'q' && playerX-1>0 && lecture_du_terrain(niveau, playerX-1, playerY) == typeCase)
	{
			res = 1;
	}
	else if(direction == 'd' && playerX+1<(niveau->nb_colonnes) && lecture_du_terrain(niveau, playerX+1, playerY) == typeCase)
	{
			res = 1;
	}
	return res;
}

//Fonction servant à vérifier que le type de case passé en paramètre correspond à la case trouver avec les coordonnés + la direction + 1 (pour la casse après la caisse) du joueur passé en paramètre
int caseDevantLaCaisse(niveau_t* niveau, char direction, int colonne, int ligne, char typeCase)
{
	int playerX = colonne;
	int playerY = ligne;
	int res = 0;
	if(direction == 'z' && playerY-2>0 && lecture_du_terrain(niveau, playerX, playerY-2) == typeCase)
	{
			res = 1;
	}
	else if(direction == 's' && playerY+2<(niveau->nb_lignes) && lecture_du_terrain(niveau, playerX, playerY+2) == typeCase)
	{
			res = 1;
	}
	else if(direction == 'q' && playerX-2>0 && lecture_du_terrain(niveau, playerX-2, playerY) == typeCase)
	{
			res = 1;
	}
	else if(direction == 'd' && playerX+2<(niveau->nb_colonnes) && lecture_du_terrain(niveau, playerX+2, playerY) == typeCase)
	{
			res = 1;
	}
	return res;
}

void avancerJoueur(niveau_t* niveau, char direction, int playerX, int playerY, char caseSuivante, char ancienneCase)
{
	place_sur_terrain(niveau, niveau->perso->colonne, niveau->perso->ligne, ' ');
	if(direction == 'z')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX, playerY-1, caseSuivante);
			niveau->perso->ligne--;
	}
	else if(direction == 's')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX, playerY+1, caseSuivante);
			niveau->perso->ligne++;
	}
	else if(direction == 'q')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX-1, playerY, caseSuivante);
			niveau->perso->colonne--;
	}
	else if(direction == 'd')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX+1, playerY, caseSuivante);
			niveau->perso->colonne++;
	}
}

void avancerJoueurEtCaisse(niveau_t* niveau, char direction, int playerX, int playerY, char caseSuivante, char ancienneCase, char caisse)
{
	place_sur_terrain(niveau, niveau->perso->colonne, niveau->perso->ligne, ' ');
	if(direction == 'z')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX, playerY-1, caseSuivante);
			place_sur_terrain(niveau, playerX, playerY-2, caisse);
			niveau->perso->ligne--;
	}
	else if(direction == 's')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX, playerY+1, caseSuivante);
			place_sur_terrain(niveau, playerX, playerY+2, caisse);
			niveau->perso->ligne++;
	}
	else if(direction == 'q')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX-1, playerY, caseSuivante);
			place_sur_terrain(niveau, playerX-2, playerY, caisse);
			niveau->perso->colonne--;
	}
	else if(direction == 'd')
	{
			place_sur_terrain(niveau, playerX, playerY, ancienneCase);
			place_sur_terrain(niveau, playerX+1, playerY, caseSuivante);
			place_sur_terrain(niveau, playerX+2, playerY, caisse);
			niveau->perso->colonne++;
	}
}
