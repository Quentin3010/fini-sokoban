#include "main.h"


int main (void)
{
	while(0!=1){
		//Écran d'accueil
		system("clear");
		char n_niveau = ' ';
		//On attend un input pour choisir le niveau voulu ou quitter le jeu
		while(n_niveau!='0' && n_niveau!='1'){
			system("clear");
			menu();
			scanf("%1c", &n_niveau);
			//Quitte le jeu si on appuye sur x
			if(n_niveau=='x'){
				system("clear");
				printf("Merci d'avoir joué !!!\n");
				return 1;
			//Affiche la table des scores
			}else if(n_niveau=='s'){
				affiche_tables_des_scores();
				sleep(5);
			}
		}
		//Initialise le niveau sélectionné
		niveau_t* niveau = lecture_du_niveau(n_niveau-'0');
        //Initialise l'historique
        historique_t* hist = nouvel_historique();
        initialiser_historique(hist);
		system("clear");
		//Démarre la partie
        char entree;
		while(niveau_termine(niveau)==0){
			printf("Tour n°%d\n\n", niveau->nb_de_pas);
			affichage_niveau(niveau);
            entree = entree_du_joueur();
            //Si le joueur fait 'a' alors il reset son action précédente
            if(entree == 'a' && hist->nb_de_tour > 0)
            {
            	//Work in progress
            }
            //Sinon, il se déplace
            else
            {
                deplacement(niveau, entree);
                niveau->nb_de_pas++;
                sauvegarde_un_coup(hist, niveau);
            }

		}
		//La partie est fini, on affiche le score du joueur
		system("clear");
		printf("VICTOIRE en %d tours\n\n", niveau->nb_de_pas);
		affichage_niveau(niveau);
		//Affiche si le score a battu le meilleur score enregistré et le sauvegarde si c'est le cas
		ecriture_du_score(n_niveau-'0', niveau->nb_de_pas);
		//Sauvegarde le score dans la table des des scores 
		sleep(1);
		//Si = 1 alors le score est meilleur que le dernier des meilleurs scores enregistrés
		if(score_check(n_niveau-'0', niveau)==1){
			printf("Quel est votre nom/pseudo pour enregistrer votre score: ");
			char* pseudo = nom_du_joueur();
			ecriture_du_multiscore(n_niveau-'0', niveau->nb_de_pas, pseudo);
			tri_du_fichier(n_niveau-'0');
			tronquer_fichier(n_niveau-'0');
		}
		//On libère le niveau de la mémoire
		liberation_du_niveau(niveau);
	}
	return 0;
	
}

void menu(void)
{
	char* chemin = "./Niveaux/accueil";
  	//Ouverture du fichier
    FILE* fichier = fopen(chemin,"r");
   	char c = getc(fichier);
   	//Lecture du fichier
   	while(c!=EOF)
   	{
   		putchar(c);
   		c = getc(fichier);
   	}
    //On ferme le fichier
	fclose(fichier);
}

void affiche_tables_des_scores(void)
{
	system("clear");
	printf("************\n");
	printf("* Niveau 0 *\n");
	printf("************\n\n");
	char* chemin = "./Plusieurs_meilleurs_scores/score_multi_0";
  	//Ouverture du fichier
    FILE* fichier = fopen(chemin,"r");
   	char c = getc(fichier);
   	//Lecture du fichier
   	while(c!=EOF)
   	{
   		putchar(c);
   		c = getc(fichier);
   	}
    //On ferme le fichier
	fclose(fichier);
	printf("\n\n************\n");
	printf("* Niveau 1 *\n");
	printf("************\n\n");
	chemin = "./Plusieurs_meilleurs_scores/score_multi_1";
  	//Ouverture du fichier
    fichier = fopen(chemin,"r");
   	c = getc(fichier);
   	//Lecture du fichier
   	while(c!=EOF)
   	{
   		putchar(c);
   		c = getc(fichier);
   	}
    //On ferme le fichier
	fclose(fichier);
}