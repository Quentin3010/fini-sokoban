#include "main.h"

niveau_t* copie_du_niveau(niveau_t* niveau)
{
	//Affectation du niveau à la mémoire
	niveau_t* sauvegarde = malloc(sizeof(niveau_t)*1);
	//Affectation des différents paramètres du terrain
	sauvegarde->nb_colonnes = niveau->nb_colonnes;
	sauvegarde->nb_lignes = niveau->nb_lignes;
	sauvegarde->terrain = malloc(sizeof(char)*sauvegarde->nb_lignes*sauvegarde->nb_colonnes);
	//Affectation des différents paramètres du joueur
	point_t* perso = malloc(sizeof(point_t)*1);
	perso->colonne = niveau->perso->colonne;
	perso->ligne = niveau->perso->ligne;
	sauvegarde->perso = perso;
	//Initialisation du nombre de pas
	sauvegarde->nb_de_pas = niveau->nb_de_pas;
	return sauvegarde;
}

historique_t* nouvel_historique()
{
	//Affectation du niveau à la mémoire
	historique_t* hist = malloc(sizeof(niveau_t*)*500);
    return hist;
}

void initialiser_historique(historique_t* hist)
{
	//Initialisation de l'historique

    hist->tableau = malloc(sizeof(niveau_t*)*314);
	hist->nb_de_tour = 0;
}

void affichage_historique(historique_t* hist)
{
    printf("%d", (hist->nb_de_tour));
	for(int i = 0; i<(hist->nb_de_tour); i++)
	{
		niveau_t* intermediaire = *(hist->tableau+i);
        affichage_niveau(intermediaire);
    }
}

void liberation_historique(niveau_t** hist)
{
	free(hist);
}

void sauvegarde_un_coup(historique_t* hist, niveau_t* niveau)
{
    hist->nb_de_tour = hist->nb_de_tour + 1;
    //printf("%d", (hist->nb_de_tour));
    //printf(hist->tableau);
    *(hist->tableau+(hist->nb_de_tour)) = niveau;
}

niveau_t* coup_precedent(historique_t* hist)
{
    niveau_t* res = *(hist->tableau+(hist->nb_de_tour));
    hist->nb_de_tour = hist->nb_de_tour - 1;
    return res;
}
